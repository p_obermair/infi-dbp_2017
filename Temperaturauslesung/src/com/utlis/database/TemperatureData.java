package com.utlis.database;
public class TemperatureData {
    private double minTemp;
    private String minDate;
    private double maxTemp;
    private String maxDate;
    private String[] monate=new String[12];
    private double[] avgTemp=new double[12];
    public void addMonth(int count, String month,double avg) {
        monate[count]=month;
        avgTemp[count]=avg;
    }
    public String[] getMonate() {
        return monate;
    }
    
    public double[] getAvgTemp() {
        return avgTemp;
    }
    public double getMinTemp() {
        return minTemp;
    }
    public void setMinTemp(double minTemp) {
        this.minTemp = minTemp;
    }
    public String getMinDate() {
        return minDate;
    }
    public void setMinDate(String minDate) {
        this.minDate = minDate;
    }
    public double getMaxTemp() {
        return maxTemp;
    }
    public void setMaxTemp(double maxTemp) {
        this.maxTemp = maxTemp;
    }
    public String getMaxDate() {
        return maxDate;
    }
    public void setMaxDate(String maxDate) {
        this.maxDate = maxDate;
    }
}
