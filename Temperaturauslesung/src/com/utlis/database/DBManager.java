
package com.utlis.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DBManager {
	Connection c;

	public DBManager() {
		try {
			try {
				Class.forName("org.sqlite.JDBC");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			String url = "jdbc:sqlite:C:\\Schule\\F�cher\\eclipse_workespace\\Temperaturauslesung\\WebContent\\fuho.db";
			c = DriverManager.getConnection(url);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public List<String> getSensors() {
		ArrayList<String> sensors = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			st = c.createStatement();
			rs = st.executeQuery("SELECT id, description FROM id_descriptions");
			sensors = new ArrayList<>();
			while (rs.next()) {
				String id = rs.getString(1);
				String description = rs.getString(2);
				sensors.add(description + " ID" + id + "_DATA");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			if (st != null)
				try {
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return sensors;
	}

	public TemperatureData getTemperatureData(String sensor) {
		PreparedStatement st = null;
		ResultSet rs = null;
		TemperatureData td = new TemperatureData();
		try {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.YEAR, -1);
			String sql = "SELECT datecolumn, MAX(value1) FROM " + sensor + " WHERE datecolumn>?";
			st = c.prepareStatement(sql);
			st.setLong(1, cal.getTimeInMillis() / 1000);
			rs = st.executeQuery();
			rs.next();

			Date d = new Date(rs.getLong(1) * 1000);
			DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

			td.setMaxDate(df.format(d));
			td.setMaxTemp(rs.getInt(2) / 100);
			st.close();
			rs.close();
			sql = "SELECT datecolumn, MIN(value1) FROM " + sensor + " WHERE datecolumn>?";
			st = c.prepareStatement(sql);
			st.setLong(1, cal.getTimeInMillis() / 1000);
			rs = st.executeQuery();
			rs.next();
			d = new Date(rs.getLong(1) * 1000);
			td.setMinDate(df.format(d));
			td.setMinTemp(rs.getInt(2) / 100);

			st.close();
			rs.close();

			cal = Calendar.getInstance();
			cal.set(Calendar.DAY_OF_MONTH, 0);
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			
			df = new SimpleDateFormat("MMMM");

			for (int i = 0; i < 12; i++) {
				long endDate = cal.getTimeInMillis() / 1000;
				cal.add(Calendar.MONTH, -1);
				long startDate = cal.getTimeInMillis() / 1000;
				
				sql = "SELECT AVG(value1) temperature FROM " + sensor + " WHERE datecolumn>? AND datecolumn <?";
				st = c.prepareStatement(sql);
				st.setLong(1, startDate);
				st.setLong(2, endDate);
				rs = st.executeQuery();
				rs.next();
				
				td.addMonth(i, df.format(new Date(endDate*1000)), rs.getDouble(1)/100);
				System.out.println(endDate + ":" + startDate);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			if (st != null)
				try {
					st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return td;
	}

	public void close() {
		try {
			c.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
