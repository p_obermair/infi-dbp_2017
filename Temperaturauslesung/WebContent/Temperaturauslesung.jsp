<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="com.utlis.database.DBManager"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Temperaturmessung</title>
<script src="Chart.js"></script>
</head>
<body>
	<h1>Temperaturmessung</h1>
	<select id="sensorselect" name="sensorselect" onchange="getSensors()">
		<option disabled selected>-- W�hle einen Sensor aus --</option>
		<%
			DBManager db = new DBManager();
			for (String sensor : db.getSensors()) {
		%>
		<option value="<%=sensor%>">
			<%=sensor%>
		</option>
		<%
			}
			db.close();
		%>
	</select>
	<div id="min-Temp"></div>
	<div id="max-Temp"></div>
	<div id="chart-container"
		style="position: relative; height: 700px; width: 600px">
		<canvas id="myChart"></canvas>
	</div>

	<script type="text/javascript">
		function getSensors() {
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (xhttp.readyState == 4 && xhttp.status == 200) {

					data = JSON.parse(this.responseText);
					document.getElementById("min-Temp").innerHTML = "Minimale Temperatur <br>"
							+ data.minTemp + "�C am " + data.minDate;
					document.getElementById("max-Temp").innerHTML = "Maximale Temperatur <br>"
							+ data.maxTemp + "�C am " + data.maxDate;
					
					plot(data.monate.reverse(),data.avgTemp.reverse())
				}
			}
			xhttp
					.open(
							"GET",
							"/Temperaturauslesung/Sensor/"
									+ document.getElementById('sensorselect').options[document
											.getElementById('sensorselect').selectedIndex].value
											.split(" ").pop(), true);
			console
					.log("/"
							+ document.getElementById('sensorselect').options[document
									.getElementById('sensorselect').selectedIndex].value
									.split(" ").pop())
			xhttp.send();
		}

	function plot(month,dataa){
		document.getElementById("myChart").remove();
		document.getElementById("chart-container").innerHTML="<canvas id='myChart'></canvas>";
		 var ctx = document.getElementById("myChart").getContext("2d");
		var myChart = new Chart(ctx,
				{
					type : 'bar',
					responsive : "false",
					data : {
						labels : month,
						datasets : [ {
							label : 'Temperature',
							backgroundColor: ["#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd","#3e95cd",],
							data :dataa,
							borderWidth : 1
						} ]
					},
					options : {
						scales : {
							yAxes : [ {
								ticks : {
									beginAtZero : true
								}
							} ]
						}
					}
				});
	}
	</script>

</body>
</html>